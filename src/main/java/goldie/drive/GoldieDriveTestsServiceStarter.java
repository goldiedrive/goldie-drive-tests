package goldie.drive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableEurekaClient
@ComponentScan(basePackages = {"rest.goldie.drive", "goldie.drive"})
public class GoldieDriveTestsServiceStarter {
    public static void main(String[] args) {
        SpringApplication.run(GoldieDriveTestsServiceStarter.class, args);
    }
}