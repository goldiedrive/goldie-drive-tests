package goldie.drive.repository;

import goldie.drive.entity.TestsEntity;
import goldie.drive.models.enums.TestType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface TestsRepo extends JpaRepository<TestsEntity, UUID> {

    List<TestsEntity> findAllByLessonId(UUID lessonId);

    List<TestsEntity> findAllBySubjectId(UUID subjectId);

    List<TestsEntity> findAllByTestType(TestType testType);

    List<TestsEntity> findAllByLessonIdAndTestType(UUID lessonId, TestType testType);
}
