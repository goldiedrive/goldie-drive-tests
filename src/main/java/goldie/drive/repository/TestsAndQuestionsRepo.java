package goldie.drive.repository;

import goldie.drive.entity.TestsAndQuestionsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TestsAndQuestionsRepo extends JpaRepository<TestsAndQuestionsEntity, UUID> {

    Optional<TestsAndQuestionsEntity> findByTestIdAndQuestionId(UUID testId, UUID questionId);

    Optional<List<TestsAndQuestionsEntity>> findAllByTestId(UUID tesId);

    void deleteAllByTestId(UUID testId);

    void deleteAllByQuestionId(UUID questionId);

    void deleteAllByTestIdAndQuestionId(UUID testId, UUID questionId);
}
