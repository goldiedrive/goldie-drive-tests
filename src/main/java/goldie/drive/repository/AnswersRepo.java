package goldie.drive.repository;

import goldie.drive.entity.AnswersEntity;
import goldie.drive.entity.QuestionsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AnswersRepo extends JpaRepository<AnswersEntity, UUID> {

    void deleteAllByQuestion(QuestionsEntity question);
}
