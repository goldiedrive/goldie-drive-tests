package goldie.drive.repository;

import goldie.drive.entity.QuestionsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface QuestionsRepo extends JpaRepository<QuestionsEntity, UUID> {

}
