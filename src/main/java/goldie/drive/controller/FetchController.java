package goldie.drive.controller;

import goldie.drive.entity.QuestionsEntity;
import goldie.drive.entity.TestsEntity;
import goldie.drive.models.DrivingTestTo;
import goldie.drive.models.enums.TestType;
import goldie.drive.service.FetchDrivingTestService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/goldie-drive-tests/fetch")
@RequiredArgsConstructor
@Validated
public class FetchController {

    private final FetchDrivingTestService fetchDrivingTestService;

    /**
     * Получение теста по id.
     *
     * @param testId uuid теста
     * @return тест с впросами и ответами.
     */
    @GetMapping("testById")
    public ResponseEntity<DrivingTestTo> getTestById(@NotNull @RequestParam UUID testId) {
        return ResponseEntity.ok(fetchDrivingTestService.getTestById(testId));
    }

    /**
     * Получение описания и uuid всех тестов.
     *
     * @return список тестов
     */
    @GetMapping("allTests")
    public ResponseEntity<List<TestsEntity>> getAllTests() {
        return ResponseEntity.ok(fetchDrivingTestService.getAllTests());
    }

    /**
     * Получение вопроса по id.
     *
     * @param questionId uuid вопроса
     * @return вопрос
     */
    @GetMapping("questionById")
    public ResponseEntity<QuestionsEntity> getQuestionById(@NotNull @RequestParam UUID questionId) {
        return ResponseEntity.ok(fetchDrivingTestService.getQuestionById(questionId));
    }

    /**
     * Получение описания и uuid всех вопросов.
     *
     * @return вопросы
     */
    @GetMapping("allQuestions")
    public ResponseEntity<List<QuestionsEntity>> getAllQuestions() {
        return ResponseEntity.ok(fetchDrivingTestService.getAllQuestions());
    }

    /**
     * Получение тестов по lessonId.
     *
     * @param lessonId uuid урока
     * @return список тестов
     */
    @GetMapping("testsByLessonId")
    public ResponseEntity<List<DrivingTestTo>> getTestsByLessonId(@RequestParam UUID lessonId) {
        return ResponseEntity.ok(fetchDrivingTestService.getTestsByLessonId(lessonId));
    }

    /**
     * Получение тестов по subjectId.
     *
     * @param subjectId uuid предмета
     * @return список тестов
     */
    @GetMapping("testsBySubjectId")
    public ResponseEntity<List<DrivingTestTo>> getTestsBySubjectId(@RequestParam UUID subjectId) {
        return ResponseEntity.ok(fetchDrivingTestService.getTestsBySubjectId(subjectId));
    }

    /**
     * Получение тестов по его типу.
     *
     * @param testType тип теста
     * @return список тестов
     */
    @GetMapping("testsByTestType")
    public ResponseEntity<List<DrivingTestTo>> getTestsByTestType(@NotNull @RequestParam TestType testType) {
        return ResponseEntity.ok(fetchDrivingTestService.getTestsByTestType(testType));
    }

    /**
     * Получение тестов по lessonId и его типу
     *
     * @param lessonId uuid теста
     * @param testType тип теста
     * @return список тестов
     */
    @GetMapping("testsByLessonIdAndTestType")
    public ResponseEntity<List<DrivingTestTo>> getTestsByLessonIdAndTestType(
        @NotNull @RequestParam UUID lessonId,
        @NotNull @RequestParam TestType testType) {
        return ResponseEntity.ok(fetchDrivingTestService.getTestsByLessonIdAndTestType(lessonId, testType));
    }
}
