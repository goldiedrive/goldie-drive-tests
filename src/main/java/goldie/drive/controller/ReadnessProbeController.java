package goldie.drive.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/")
public class ReadnessProbeController {

    @GetMapping
    public ResponseEntity<String> readinessProbe() {
        return ResponseEntity.ok("Goldie Drive Tests Service OK");
    }
}
