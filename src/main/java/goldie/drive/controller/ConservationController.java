package goldie.drive.controller;

import goldie.drive.entity.QuestionsEntity;
import goldie.drive.entity.TestsAndQuestionsEntity;
import goldie.drive.entity.TestsEntity;
import goldie.drive.models.DrivingAnswerTo;
import goldie.drive.service.ConservationDrivingTestService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/goldie-drive-tests/save")
@RequiredArgsConstructor
@Validated
public class ConservationController {

    private final ConservationDrivingTestService conservationDrivingTestService;

    /**
     * Создание и обновление тестов.
     *
     * @param tests тесты
     * @return тесты
     */
    @PostMapping("tests")
    public ResponseEntity<List<TestsEntity>> saveTests(@Valid @RequestBody List<TestsEntity> tests) {
        return ResponseEntity.ok(conservationDrivingTestService.saveTests(tests));
    }

    /**
     * Создание и обновление вопросов.
     *
     * @param questions вопросы
     * @return вопросы
     */
    @PostMapping("questions")
    public ResponseEntity<List<QuestionsEntity>> saveQuestions(@Valid @RequestBody List<QuestionsEntity> questions) {
        return ResponseEntity.ok(conservationDrivingTestService.saveQuestions(questions));
    }

    /**
     * Создание и обновление ответов.
     *
     * @param answers ответы
     * @return ответы
     */
    @PostMapping("answers")
    public ResponseEntity<List<DrivingAnswerTo>> saveAnswers(@Valid @RequestBody List<DrivingAnswerTo> answers) {
        return ResponseEntity.ok(conservationDrivingTestService.saveAnswers(answers));
    }

    /**
     * Создание и обновление связей между вопросами и тестами.
     *
     * @param entities TestsAndQuestionsEntity
     * @return список TestsAndQuestionsEntity
     */
    @PostMapping("connect-test-and-question")
    public ResponseEntity<List<TestsAndQuestionsEntity>> connectTestAndQuestion(
        @Valid @RequestBody List<TestsAndQuestionsEntity> entities) {
        return ResponseEntity.ok(conservationDrivingTestService.connectTestAndQuestion(entities));
    }
}
