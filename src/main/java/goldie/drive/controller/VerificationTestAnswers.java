package goldie.drive.controller;

import goldie.drive.models.DrivingTestTo;
import goldie.drive.service.VerificationTestAnswersService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/goldie-drive-tests/verification")
@RequiredArgsConstructor
@Validated
public class VerificationTestAnswers {

    private final VerificationTestAnswersService verificationTestAnswersService;

    /**
     * Проверка ответов теста на корректность.
     *
     * @param drivingTestTo тест с вопросами и ответами
     */
    @PostMapping("test")
    public ResponseEntity<?> checkTestAnswers(@NotNull @RequestBody DrivingTestTo drivingTestTo) {
        verificationTestAnswersService.checkTestAnswers(drivingTestTo);
        return ResponseEntity.ok().build();
    }
}
