package goldie.drive.controller;

import goldie.drive.entity.TestsAndQuestionsEntity;
import goldie.drive.service.RemoveDrivingTestService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/goldie-drive-tests/remove")
@RequiredArgsConstructor
@Validated
public class RemoveController {

    private final RemoveDrivingTestService removeDrivingTestService;

    /**
     * Удаление тестов.
     *
     * @param tests список тестов
     */
    @DeleteMapping ("tests")
    public ResponseEntity<?> removeTests(@RequestBody List<UUID> tests) {
        removeDrivingTestService.removeTests(tests);
        return ResponseEntity.ok().build();
    }

    /**
     * Удаление вопросов.
     *
     * @param questions список вопросов
     */
    @DeleteMapping("questions")
    public ResponseEntity<?> removeQuestions(@RequestBody List<UUID> questions) {
        removeDrivingTestService.removeQuestions(questions);
        return ResponseEntity.ok().build();
    }

    /**
     * Удаление ответов.
     *
     * @param answers список ответов
     */
    @DeleteMapping("answers")
    public ResponseEntity<?> removeAnswers(@RequestBody List<UUID> answers) {
        removeDrivingTestService.removeAnswers(answers);
        return ResponseEntity.ok().build();
    }

    /**
     * Удаление связей между тестами и вопросами.
     *
     * @param connections список связей
     */
    @DeleteMapping("connect-test-and-question")
    public ResponseEntity<?> removeConnectionTestAndQuestion(
        @Valid @RequestBody List<TestsAndQuestionsEntity> connections) {
        removeDrivingTestService.removeConnectionTestAndQuestion(connections);
        return ResponseEntity.ok().build();
    }
}
