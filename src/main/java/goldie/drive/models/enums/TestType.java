package goldie.drive.models.enums;

public enum TestType {
    /**
     * Экзамен.
     */
    EXAM,
    /**
     * Тест для урока и для билетов ПДД.
     */
    SIMPLE_TEST,
    /**
     * Зачет.
     */
    CREDIT;
}
