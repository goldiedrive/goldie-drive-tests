package goldie.drive.models;

import goldie.drive.models.enums.TestType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Data
@Accessors(chain = true)
@Builder
@AllArgsConstructor
public class DrivingTestTo {

    @NotNull
    private UUID testId;
    private String testDescription;
    private TestType testType;
    private UUID lessonId;
    private UUID subjectId;
    private List<DrivingQuestionTo> questions;
}
