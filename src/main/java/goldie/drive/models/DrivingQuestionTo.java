package goldie.drive.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
public class DrivingQuestionTo {

    private UUID questionId;
    private String questionDescription;
    private List<DrivingAnswerTo> answers;
}
