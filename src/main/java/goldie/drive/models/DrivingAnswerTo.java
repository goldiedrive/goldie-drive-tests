package goldie.drive.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class DrivingAnswerTo {
    private UUID answerId;
    private String answerDescription;
    private Boolean isCorrectAnswer;
    private UUID questionId;
}
