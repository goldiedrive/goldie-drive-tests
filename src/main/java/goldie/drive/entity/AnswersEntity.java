package goldie.drive.entity;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;
import static javax.persistence.GenerationType.AUTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Table(name = "answers")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class AnswersEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    @Column(name = "answer_id")
    private UUID answerId;

    @Column(name = "answer_description")
    @NotEmpty
    private String answerDescription;

    @JsonProperty(access = WRITE_ONLY)
    @Column(name = "is_correct_answer")
    @NotNull
    private Boolean isCorrectAnswer;

    @NotNull
    @JsonIgnore
    @JoinColumn(name = "question_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private QuestionsEntity question;
}
