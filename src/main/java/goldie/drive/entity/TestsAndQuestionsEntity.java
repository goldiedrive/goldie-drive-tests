package goldie.drive.entity;

import static javax.persistence.GenerationType.AUTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Table(name = "tests_and_questions")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class TestsAndQuestionsEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    @JsonIgnore
    private UUID id;

    @Column(name = "test_id")
    @NotNull
    private UUID testId;

    @Column(name = "question_id")
    @NotNull
    private UUID questionId;
}
