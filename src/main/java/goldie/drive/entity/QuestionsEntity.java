package goldie.drive.entity;

import static javax.persistence.GenerationType.AUTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "questions")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class QuestionsEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    @Column(name = "question_id")
    private UUID questionId;

    @Column(name = "question_description")
    @NotEmpty
    private String questionDescription;

    @OneToMany(mappedBy = "question", fetch = FetchType.EAGER)
    private Set<AnswersEntity> answers;
}
