package goldie.drive.entity;

import static javax.persistence.GenerationType.AUTO;

import goldie.drive.models.enums.TestType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Table(name = "tests")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class TestsEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    @Column(name = "test_id")
    private UUID testId;

    @Column(name = "test_description")
    @NotEmpty
    private String testDescription;

    @Enumerated(EnumType.STRING)
    @Column(name = "test_type")
    @NotNull
    private TestType testType;

    @Column(name = "lesson_id")
    private UUID lessonId;

    @Column(name = "subject_id")
    private UUID subjectId;
}
