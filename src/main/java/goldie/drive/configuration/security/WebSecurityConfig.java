package goldie.drive.configuration.security;

import static rest.goldie.drive.model.enums.UserRoles.ADMIN;
import static rest.goldie.drive.model.enums.UserRoles.STUDENT;

import goldie.drive.jwt.utils.JwtFilter;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@AllArgsConstructor
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtFilter jwtFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()

            .antMatchers("/").permitAll()

            .antMatchers("/goldie-drive-tests/save/**").hasAnyAuthority(ADMIN.name())

            .antMatchers("/goldie-drive-tests/fetch/**").hasAnyAuthority(ADMIN.name(), STUDENT.name())

            .antMatchers("/goldie-drive-tests/remove/**").hasAnyAuthority(ADMIN.name())

            .antMatchers("/goldie-drive-tests/verification/**").hasAnyAuthority(ADMIN.name(), STUDENT.name())

            .antMatchers("/**").denyAll()

            .and().exceptionHandling()
            .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
