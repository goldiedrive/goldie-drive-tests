package goldie.drive.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import goldie.drive.entity.QuestionsEntity;
import goldie.drive.entity.TestsEntity;
import goldie.drive.models.DrivingQuestionTo;
import goldie.drive.models.DrivingTestTo;
import goldie.drive.models.enums.TestType;
import goldie.drive.repository.QuestionsRepo;
import goldie.drive.repository.TestsAndQuestionsRepo;
import goldie.drive.repository.TestsRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class FetchDrivingTestService {

    private final TestsRepo testsRepo;
    private final TestsAndQuestionsRepo testsAndQuestionsRepo;
    private final QuestionsRepo questionsRepo;
    private final ObjectMapper objectMapper;

    public DrivingTestTo getTestById(UUID testId) {
        TestsEntity testsEntity = getTestsEntityById(testId);
        return getDrivingTestToByTestsEntity(testsEntity);
    }

    public TestsEntity getTestsEntityById(UUID testId) {
        return testsRepo
            .findById(testId)
            .orElseThrow(() -> new RuntimeException("not found such testId"));
    }

    private DrivingTestTo getDrivingTestToByTestsEntity(TestsEntity testsEntity) {
        DrivingTestTo drivingTestTo = DrivingTestTo.builder()
            .testId(testsEntity.getTestId())
            .testDescription(testsEntity.getTestDescription())
            .testType(testsEntity.getTestType())
            .lessonId(testsEntity.getLessonId())
            .subjectId(testsEntity.getSubjectId())
            .questions(new ArrayList<>())
            .build();

        testsAndQuestionsRepo.findAllByTestId(testsEntity.getTestId())
            .ifPresent(testsAndQuestions -> {
                testsAndQuestions.forEach(testAndQuestion -> {
                    drivingTestTo.getQuestions().add(
                        objectMapper.convertValue(
                            questionsRepo.findById(testAndQuestion.getQuestionId()).get(),
                            DrivingQuestionTo.class
                        )
                    );
                });
            });

        return drivingTestTo;
    }

    public List<TestsEntity> getAllTests() {
        return testsRepo.findAll();
    }

    public QuestionsEntity getQuestionById(UUID id) {
        return questionsRepo
            .findById(id)
            .orElseThrow(() -> new RuntimeException("not found such questionId"));
    }

    public List<QuestionsEntity> getAllQuestions() {
        return questionsRepo.findAll();
    }

    public List<DrivingTestTo> getTestsByLessonId(UUID lessonId) {
        List<TestsEntity> tests = testsRepo.findAllByLessonId(lessonId);
        return getListDrivingTestToByListTestsEntity(tests);
    }

    public List<DrivingTestTo> getTestsBySubjectId(UUID subjectId) {
        List<TestsEntity> tests = testsRepo.findAllBySubjectId(subjectId);
        return getListDrivingTestToByListTestsEntity(tests);
    }

    public List<DrivingTestTo> getTestsByTestType(TestType testType) {
        List<TestsEntity> tests = testsRepo.findAllByTestType(testType);
        return getListDrivingTestToByListTestsEntity(tests);
    }

    public List<DrivingTestTo> getTestsByLessonIdAndTestType(UUID lessonId, TestType testType) {
        List<TestsEntity> tests = testsRepo.findAllByLessonIdAndTestType(lessonId, testType);
        return getListDrivingTestToByListTestsEntity(tests);
    }

    private List<DrivingTestTo> getListDrivingTestToByListTestsEntity(List<TestsEntity> tests) {
        List<DrivingTestTo> drivingTestToList = new ArrayList<>();

        tests.forEach(test -> drivingTestToList.add(getDrivingTestToByTestsEntity(test)));

        return drivingTestToList;
    }
}
