package goldie.drive.service;

import goldie.drive.entity.QuestionsEntity;
import goldie.drive.entity.TestsAndQuestionsEntity;
import goldie.drive.repository.AnswersRepo;
import goldie.drive.repository.QuestionsRepo;
import goldie.drive.repository.TestsAndQuestionsRepo;
import goldie.drive.repository.TestsRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class RemoveDrivingTestService {

    private final TestsRepo testsRepo;
    private final QuestionsRepo questionsRepo;
    private final AnswersRepo answersRepo;
    private final TestsAndQuestionsRepo testsAndQuestionsRepo;

    @Transactional
    public void removeTests(List<UUID> tests) {
        // удаляем связи в таблице tests_and_questions
        tests.forEach(testsAndQuestionsRepo::deleteAllByTestId);

        // удаляем тесты в талице tests
        tests.forEach(testsRepo::deleteById);
    }

    @Transactional
    public void removeQuestions(List<UUID> questions) {
        // удаляем связи в таблице tests_and_questions
        questions.forEach(testsAndQuestionsRepo::deleteAllByQuestionId);

        // удаляем все ответы на вопросы
        questions.forEach(id -> answersRepo.deleteAllByQuestion(getQuestionById(id)));

        // удаляем вопросы в таблице questions
        questions.forEach(questionsRepo::deleteById);
    }

    @Transactional
    public void removeAnswers(List<UUID> answers) {
        // удаляем ответы из answers
        answers.forEach(answersRepo::deleteById);
    }

    @Transactional
    public void removeConnectionTestAndQuestion(List<TestsAndQuestionsEntity> connections) {
        // удаляем все связи
        connections.forEach(connection -> testsAndQuestionsRepo
            .deleteAllByTestIdAndQuestionId(connection.getTestId(), connection.getQuestionId()));
    }

    private QuestionsEntity getQuestionById(UUID questionId) {
        return questionsRepo
            .findById(questionId)
            .orElseThrow(() -> new RuntimeException("not found such questionId " + questionId));
    }
}
