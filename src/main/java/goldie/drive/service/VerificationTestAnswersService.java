package goldie.drive.service;

import goldie.drive.entity.AnswersEntity;
import goldie.drive.entity.TestsEntity;
import goldie.drive.models.DrivingAnswerTo;
import goldie.drive.models.DrivingTestTo;
import goldie.drive.repository.QuestionsRepo;
import goldie.drive.repository.TestsAndQuestionsRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class VerificationTestAnswersService {

    private final static String ANSWERS_NOT_CORRECT = "answers not correct";

    private final TestsAndQuestionsRepo testsAndQuestionsRepo;
    private final QuestionsRepo questionsRepo;
    private final FetchDrivingTestService fetchDrivingTestService;

    public void checkTestAnswers(DrivingTestTo drivingTestTo) {
        // находим тест в БД
        TestsEntity testsEntity = fetchDrivingTestService.getTestsEntityById(drivingTestTo.getTestId());

        // создадим список ответов для найденного теста в БД
        List<AnswersEntity> answersFromDB = getAnswersFromDB(testsEntity);

        // создадим список ответов из полученных данных от пользователя
        List<DrivingAnswerTo> answerFromUser = getAnswersFromUser(drivingTestTo);

        // сравним ответы из БД и полученные от пользователя
        if (!isEqualsAnswersFromDBAndUserAnswers(answersFromDB, answerFromUser)) {
            // TODO отправить в профайлер результаты теста
            throw new RuntimeException(ANSWERS_NOT_CORRECT);
        }

        // TODO отправить в профайлер результаты теста
    }

    private boolean isEqualsAnswersFromDBAndUserAnswers(List<AnswersEntity> answersFromDB,
                                                        List<DrivingAnswerTo> answersFromUser) {
        // сравниваем количество ответов
        if (answersFromDB.size() != answersFromUser.size()) {
            return false;
        }

        // сравниваем эквивалентность ответов по uuid
        List<UUID> answersFromDBId = answersFromDB.stream()
            .map(AnswersEntity::getAnswerId)
            .collect(Collectors.toList());

        List<UUID> answersFromUserId = answersFromUser.stream()
            .map(DrivingAnswerTo::getAnswerId)
            .collect(Collectors.toList());

        if (!answersFromUserId.containsAll(answersFromDBId)) {
            return false;
        }

        // сравнием ответы на корректность
        for (AnswersEntity answerFromDB : answersFromDB) {
            for (DrivingAnswerTo answerFromUser : answersFromUser) {
                if (answerFromDB.getAnswerId().equals(answerFromUser.getAnswerId())
                    && !answerFromDB.getIsCorrectAnswer().equals(answerFromUser.getIsCorrectAnswer())) {
                    return false;
                }
            }
        }

        return true;
    }

    private List<DrivingAnswerTo> getAnswersFromUser(DrivingTestTo drivingTestTo) {
        List<DrivingAnswerTo> answerFromUser = new ArrayList<>();

        drivingTestTo.getQuestions().forEach(question -> {
            answerFromUser.addAll(question.getAnswers());
        });

        return answerFromUser;
    }

    private List<AnswersEntity> getAnswersFromDB(TestsEntity testsEntity) {
        List<AnswersEntity> answersFromDB = new ArrayList<>();

        testsAndQuestionsRepo.findAllByTestId(testsEntity.getTestId())
            .ifPresent(testsAndQuestions -> {
                    testsAndQuestions.forEach(testAndQuestion ->
                        answersFromDB.addAll(
                            questionsRepo
                                .findById(testAndQuestion.getQuestionId()).get()
                                .getAnswers()
                        )
                    );
                }
            );

        return answersFromDB;
    }
}
