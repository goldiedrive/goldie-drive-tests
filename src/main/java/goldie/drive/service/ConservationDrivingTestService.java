package goldie.drive.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import goldie.drive.entity.AnswersEntity;
import goldie.drive.entity.QuestionsEntity;
import goldie.drive.entity.TestsAndQuestionsEntity;
import goldie.drive.entity.TestsEntity;
import goldie.drive.models.DrivingAnswerTo;
import goldie.drive.repository.AnswersRepo;
import goldie.drive.repository.QuestionsRepo;
import goldie.drive.repository.TestsAndQuestionsRepo;
import goldie.drive.repository.TestsRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ConservationDrivingTestService {

    private final TestsRepo testsRepo;
    private final QuestionsRepo questionsRepo;
    private final AnswersRepo answersRepo;
    private final TestsAndQuestionsRepo testsAndQuestionsRepo;
    private final ObjectMapper objectMapper;

    @Transactional
    public List<TestsEntity> saveTests(List<TestsEntity> tests) {
        return testsRepo.saveAll(tests);
    }

    @Transactional
    public List<QuestionsEntity> saveQuestions(List<QuestionsEntity> questions) {
        return questionsRepo.saveAll(questions);
    }

    @Transactional
    public List<DrivingAnswerTo> saveAnswers(List<DrivingAnswerTo> answers) {
        List<AnswersEntity> answersEntities = new ArrayList<>();

        answers.forEach(answer -> {
            QuestionsEntity questionsEntity = questionsRepo.findById(answer.getQuestionId())
                .orElseThrow(() -> new RuntimeException("not found such questionId: " + answer.getQuestionId().toString()));
            answersEntities.add(
                objectMapper.convertValue(answer, AnswersEntity.class)
                .setQuestion(questionsEntity)
            );
        });

        return answersRepo.saveAll(answersEntities).stream()
            .map(entity ->
                new DrivingAnswerTo(
                    entity.getAnswerId(),
                    entity.getAnswerDescription(),
                    entity.getIsCorrectAnswer(),
                    entity.getQuestion().getQuestionId()
                )
            )
            .collect(Collectors.toList());
    }

    @Transactional
    public List<TestsAndQuestionsEntity> connectTestAndQuestion(List<TestsAndQuestionsEntity> entities) {
        return testsAndQuestionsRepo.saveAll(entities.stream()
            .filter(entity -> testsAndQuestionsRepo
                .findByTestIdAndQuestionId(entity.getTestId(), entity.getQuestionId())
                .isEmpty())
            .collect(Collectors.toList()));
    }
}
