create table tests
(
    test_id          uuid primary key,
    test_description varchar not null,
    test_type        varchar not null,
    lesson_id        uuid,
    subject_id       uuid
);