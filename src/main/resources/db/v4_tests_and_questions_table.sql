create table tests_and_questions (
    id uuid primary key,
    test_id uuid references tests (test_id) not null ,
    question_id uuid references questions (question_id) not null
);