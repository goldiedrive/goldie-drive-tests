create table questions
(
    question_id          uuid primary key,
    question_description varchar not null
);