create table answers
(
    answer_id   uuid primary key,
    answer_description varchar not null,
    is_correct_answer  boolean not null,
    question_id uuid references questions (question_id) not null
);